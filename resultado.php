<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resultado</title>
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="titulo_segundo">
    <?php

session_name("quiz");
session_start();

if (isset($_REQUEST['comida10'])) {
    if ($_REQUEST['comida10'] == 'tacos') 
    $_SESSION['cont']++;
} else {
    header("Location:http://localhost/php_quiz/php/");
}

if($_SESSION['cont'] <= 5){
    echo "<img class='imagen' src='img/perdedor.jpeg'>";
    echo "<br>";
    echo "<br>";
    echo "Suerte la proxima vez";
    echo "<br>";
} else if ($_SESSION['cont'] > 5){
    echo "<img class='imagen' src='img/ganador.jpeg'>";
    echo "<br>";
    echo "<br>";
    echo "<p> eres un ganador";
    echo "<br>";
}
?>
    </div>

    <h2 class="titulofin">¡HASTA PRONTO!</h2>
</body>

</html